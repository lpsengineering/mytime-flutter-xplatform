import 'package:flutter/material.dart';
import 'containers/Login/index.dart';
import 'containers/Home/index.dart';
import 'containers/EditTimeLog/index.dart';
import 'package:mytime_mobile/containers/Login/processLogin.dart';
import 'package:mytime_mobile/utils/mood_meter/main.dart';
import 'package:mytime_mobile/services/globals.dart' as globals;
import 'package:mytime_mobile/models/user.dart';
import 'package:appcenter/appcenter.dart';
import 'package:appcenter_analytics/appcenter_analytics.dart';
import 'package:appcenter_crashes/appcenter_crashes.dart';

class MTApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    globals.currentUser = UserProfile();
    initAppCenter();
    return MaterialApp(
      title: 'MyTime Mobile',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: 'Quicksand'
      ),
      home: LoginPage(),
      routes:  <String, WidgetBuilder>{
        '/login': (BuildContext context) => LoginPage(),
        '/home': (BuildContext context) => HomePage(),
        '/editTimeLog': (BuildContext context) => EditTimeLogPage(),
        '/moodMeter': (BuildContext context) => MyReviewPage(timesheet: null,),
      },
    );
  }

  initAppCenter() async {
    var key = '0c65ed68-d2fe-4e7f-a3d6-4ff9f1bdad17';

    await AppCenter.start(key, [AppCenterAnalytics.id, AppCenterCrashes.id]);
    await AppCenter.setEnabled(true);
    await AppCenterCrashes.setEnabled(true);
    await AppCenterAnalytics.setEnabled(true);
  }
}

void main() => runApp(MTApp());