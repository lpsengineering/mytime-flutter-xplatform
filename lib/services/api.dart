import 'dart:io';
import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:mytime_mobile/models/auth.dart';
import 'package:mytime_mobile/models/user.dart';
import 'package:mytime_mobile/services/prefs.dart';
import 'package:mytime_mobile/models/timesheet.dart';
import 'package:mytime_mobile/models/placement_code.dart';
import 'package:mytime_mobile/models/placement.dart';

const apiClientId = 'cHbOAAKDptMFnPnEWDOSWJwoVZuySMVXthFY'
    'XNHqnLLrxlFPRrdSSoDkWnrPOPeAlsAMHwNahaRQvhfFvQROE';
const apiSecret = '27037399992987747329';

Future<SfAuthResponse> loginWithSFToken({@required String username,
  @required String password, @required String securityCode}) async {
  String url = 'https://test.salesforce.com/services/oauth2/'
      'token?grant_type=password&client_id=$apiClientId&client_secret='
      '$apiSecret&username=$username&password=$password$securityCode';
  final response = await post(url,
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json'
      },
  );
  return sfAuthResponseFromJson(response.body);
}

Future<UserProfile> getUserDetails() async {
  SfAuthResponse userDetails = await getAuthFromPrefs();
  String url = '${userDetails.instanceUrl}/services/data/'
  'v44.0/query/?q=SELECT+AboutMe%2CAccountId%2CAddress%2CAlias%2CBadgeText%2C'
  'BannerPhotoUrl%2CCallCenterId%2CCity%2CCommunityNickname%2CCompanyName%'
  '2CContactId%2CContact_Id__c%2CContact_Record__c%2CCountry%2CCreatedById%2C'
  'CreatedDate%2CDelegatedApproverId%2CDepartment%2CDigestFrequency%2CDivision'
  '%2CEmail%2CEmailEncodingKey%2CEmailPreferencesAutoBcc%2C'
  'EmailPreferencesAutoBccStayInTouch%2CEmailPreferencesStayInTouchReminder'
  '%2CEmployeeNumber%2CExtension%2CFax%2CFederationIdentifier%2CFirstName%'
  '2CForecastEnabled%2CFullPhotoUrl%2CGeocodeAccuracy%2CId%2CIsActive%2C'
  'IsExtIndicatorVisible%2CIsPortalEnabled%2CIsProfilePhotoActive%2C'
  'LanguageLocaleKey%2CLastLoginDate%2CLastModifiedById%2CLastModifiedDate%2C'
  'LastName%2CLastReferencedDate%2CLastViewedDate%2CLatitude%2CLocaleSidKey'
  '%2CLongitude%2CManagerId%2CMediumBannerPhotoUrl%2CMediumPhotoUrl%2C'
  'MiddleName%2CMobilePhone%2CName%2COfflinePdaTrialExpirationDate%2C'
  'OfflineTrialExpirationDate%2COutOfOfficeMessage%2CPhone%2CPortalRole%2C'
  'PostalCode%2CReceivesAdminInfoEmails%2CReceivesInfoEmails%2CSenderEmail%2C'
  'SenderName%2CSignature%2CSmallBannerPhotoUrl%2CSmallPhotoUrl%2CState%2C'
  'StayInTouchNote%2CStayInTouchSignature%2CStayInTouchSubject%2CStreet%2C'
  'Suffix%2CSystemModstamp%2CTimeZoneSidKey%2CTitle%2CUsername%2CUserRoleId%2C'
  'UserType+FROM+User+where+Username+%3D+%27${userDetails.username}%27';
  final response = await get(url,
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer ${userDetails.accessToken}'
    },
  );
  return userProfileFromJson(response.body);
}

Future<Timesheet> getTimesheetForWeekEnd(String weekEndDate, {
  @required String userId
  }) async {
  SfAuthResponse userDetails = await getAuthFromPrefs();
  String url = '${userDetails.instanceUrl}/services/data/'
      'v44.0/query/?q=SELECT+Buyer_Generated_Invoice__c%2CCandidate__c%2C'
      'Case__c%2CClient_Code__c%2CComment__c%2CCreatedById%2CCreatedDate%2C'
      'Description__c%2CEmployee__c%2CExpected_Hours__c%2CFri__c%2C'
      'Holiday_Name__c%2CId%2CInvoiceStatus__c%2CIsDeleted%2CLastActivityDate'
      '%2CLastModifiedById%2CLastModifiedDate%2CLastReferencedDate%2C'
      'LastViewedDate%2CLogged_Hours__c%2CLPS_Manager__c%2CMon__c%2CName%2C'
      'OwnerId%2CPlacement__c%2CQuantity__c%2CRate_Type__c%2CRecordTypeId%2C'
      'Sat__c%2CService_Delivery_Manager_Reporting__c%2CStatus__c%2CSun__c%2C'
      'SystemModstamp%2CThu__c%2CTimesheet_Hours__c%2CTue__c%2CUnit_Type__c%2C'
      'Utilisation_Difference__c%2CUtilisation__c%2CWed__c%2CWeek_End_Date__c'
      '%2CX24_Hours_Passed__c+FROM+Timesheet__c+WHERE+Week_End_Date__c'
      '+%3D+$weekEndDate+AND+OwnerId+%3D+%27$userId%27';
  final response = await get(url,
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer ${userDetails.accessToken}'
    },
  );
  return timesheetFromJson(response.body);
}

Future<List<PlacementCode>> getPlacementCodes() async {
  SfAuthResponse userDetails = await getAuthFromPrefs();
  String url = '${userDetails.instanceUrl}/services/data/'
      'v44.0/query/?q=SELECT+CreatedById%2CCreatedDate%2CId%2CIsDeleted%2C'
      'LastModifiedById%2CLastModifiedDate%2CName%2CPlacement__c%2C'
      'SystemModstamp+FROM+Placement_Code__c';
  final response = await get(url,
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer ${userDetails.accessToken}'
    },
  );
  return placementCodesFromJson(response.body);
}

Future<List<Placement>> getPlacements({@required contactId}) async {
  SfAuthResponse userDetails = await getAuthFromPrefs();
  String url = '${userDetails.instanceUrl}/services/data/'
      'v44.0/query/?q=SELECT+Active_Staff_Member__c%2C'
      'Affinity_Employee_Number__c%2CClient_Line_Manager__c%2CContact_ID__c%2C'
      'Contract_Notice_Period_Offer__c%2CContract_Notice_Period_Unit_Offer__c'
      '%2CCreatedById%2CCreatedDate%2CEnd_Date_Offer__c%2C'
      'Expected_Logged_Hours__c%2CId%2CInternal_or_Non_Billable_Job__c'
      '%2CIsDeleted%2CLastActivityDate%2CLastModifiedById%2CLastModifiedDate%2C'
      'LastReferencedDate%2CLastViewedDate%2C'
      'Latest_Timesheet_Week_End_Date_Check__c%2C'
      'Latest_Timesheet_Week_End_Date__c%2CLocation_Offer__c%2C'
      'LPS_Executive_Owner__c%2CName%2CPay_Rate_Offer__c%2CPlacement_Name__c%2C'
      'Placement_Total_Hours__c%2CPO_Number__c%2CProject_Name_Offer__c%2C'
      'Rate_Type_Offer__c%2CRate_Type__c%2CRecordTypeId%2CRelated_Offer__c%2C'
      'SoW_Contract_End_Date_Offer__c%2CSoW_Contract_Start_Date_Offer__c%2C'
      'Staff_Type__c%2CStart_Date_Offer__c%2CSystemModstamp%2C'
      'Target_Weekly_Work_Hours2__c%2CTimesheet_Approver_Offer__c%2C'
      'Timesheet_Approver__c%2CTotalWeeklyWorkhoursOver__c%2C'
      'TotalWeeklyWorkhoursUnder__c%2Cts2extams__Action_Buttons__c%2C'
      'ts2extams__Communication_Buttons__c%2Cts2extams__Control_Buttons__c%2C'
      'ts2extams__Substatus__c%2Cts2__Accounts_Payable__c%2Cts2__Client__c%2C'
      'ts2__Code__c%2Cts2__Contact_Email__c%2Cts2__Contact_Phone__c%2C'
      'ts2__Contact__c%2Cts2__Employee__c%2Cts2__End_Date__c%2C'
      'ts2__Hiring_Manager__c%2Cts2__Job__c%2Cts2__Location__c%2C'
      'ts2__Pay_Rate__c%2Cts2__Start_Date__c%2Cts2__Status__c%2C'
      'ts2__Third_Party__c%2CWorking_Days__c+FROM+ts2__Placement__c+'
      'WHERE+ts2__Employee__c+%3D+%27$contactId%27';
  final response = await get(url,
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer ${userDetails.accessToken}'
    },
  );
  return placementsFromJson(response.body);
}

Future<bool> setHourForDay({
  @required String timesheetId,
  @required String day,
  @required double hours}) async {
  SfAuthResponse userDetails = await getAuthFromPrefs();
  String url = '${userDetails.instanceUrl}/services/data/v44.0/sobjects/Timesheet__c/$timesheetId';
  Map<String, dynamic> params = {
    day: hours
  };
  final response = await patch(url,
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer ${userDetails.accessToken}',
    },
    body: json.encode(params)
  );
  print(response.request.toString());
  print(params);
  if (response.statusCode == 204) {
    return true;
  }
  return false;
}

Future<bool> updateTimesheet({
  @required TimesheetRecord timesheet
  }) async {
  SfAuthResponse userDetails = await getAuthFromPrefs();
  String url = '${userDetails.instanceUrl}/services/data/v44.0/sobjects/Timesheet__c/${timesheet.id}';
  Map<String, dynamic> params = {
    'Placement__c': timesheet.placementC,
    'Client_Code__c': timesheet.clientCodeC,
    'Description__c': timesheet.descriptionC,
    'Comment__c': timesheet.commentC,
  };
  final response = await patch(url,
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        HttpHeaders.authorizationHeader: 'Bearer ${userDetails.accessToken}',
      },
      body: json.encode(params)
  );
  print(response.request.toString());
  print(params);
  if (response.statusCode == 204) {
    print('update timesheet success');
    return true;
  }
  return false;
}