// To parse this JSON data, do
//
//     final placement = placementFromJson(jsonString);

import 'dart:convert';

List<PlacementCode> placementCodesFromJson(String str) {
  List<PlacementCode> placements = List();
  final jsonData = json.decode(str);
  for(Map<String, dynamic> record in jsonData["records"]) {
    placements.add(PlacementCode.fromJson(record));
  }
  return placements;
}

String placementCodesToJson(PlacementCode data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class PlacementCode {
  String createdById;
  String createdDate;
  String id;
  bool isDeleted;
  String lastModifiedById;
  String lastModifiedDate;
  String name;
  String placementC;
  String systemModstamp;

  PlacementCode({
    this.createdById,
    this.createdDate,
    this.id,
    this.isDeleted,
    this.lastModifiedById,
    this.lastModifiedDate,
    this.name,
    this.placementC,
    this.systemModstamp,
  });

  factory PlacementCode.fromJson(Map<String, dynamic> json) => new PlacementCode(
    createdById: json["CreatedById"],
    createdDate: json["CreatedDate"],
    id: json["Id"],
    isDeleted: json["IsDeleted"],
    lastModifiedById: json["LastModifiedById"],
    lastModifiedDate: json["LastModifiedDate"],
    name: json["Name"],
    placementC: json["Placement__c"],
    systemModstamp: json["SystemModstamp"],
  );

  Map<String, dynamic> toJson() => {
    "CreatedById": createdById,
    "CreatedDate": createdDate,
    "Id": id,
    "IsDeleted": isDeleted,
    "LastModifiedById": lastModifiedById,
    "LastModifiedDate": lastModifiedDate,
    "Name": name,
    "Placement__c": placementC,
    "SystemModstamp": systemModstamp,
  };
}
