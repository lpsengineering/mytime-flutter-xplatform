// To parse this JSON data, do
//
//     final timesheet = timesheetFromJson(jsonString);
import 'package:flutter/material.dart';
import 'dart:convert';

Timesheet timesheetFromJson(String str) {
  final jsonData = json.decode(str);
  return Timesheet.fromJson(jsonData);
}

String timesheetToJson(Timesheet data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class Timesheet {
  List<TimesheetRecord> records;

  Timesheet({
    this.records,
  });

  factory Timesheet.fromJson(Map<String, dynamic> json) => new Timesheet(
        records: new List<TimesheetRecord>.from(json["records"].map((x) => TimesheetRecord.fromJson(x)))
    );

  Map<String, dynamic> toJson() => {
    "records": new List<dynamic>.from(records.map((x) => x.toJson())),
  };

  double getTotalHours() {
    double hours = 0;
    this.records.forEach((record) {
      hours += record.sunC;

      print(hours.toString());
      hours += record.monC;

      print(hours.toString());
      hours += record.tueC;

      print(hours.toString());
      hours += record.wedC;

      print(hours.toString());
      hours += record.thuC;

      print(hours.toString());
      hours += record.friC;

      print(hours.toString());
      hours += record.satC;

      print(hours.toString());
      print(record.clientCodeC);
    });
    return hours;
  }
}

class TimesheetRecord {
  dynamic buyerGeneratedInvoiceC;
  String candidateC;
  dynamic caseC;
  String clientCodeC;
  String commentC;
  String createdById;
  String createdDate;
  String descriptionC;
  String employeeC;
  double expectedHoursC;
  double friC;
  dynamic holidayNameC;
  String id;
  String invoiceStatusC;
  bool isDeleted;
  dynamic lastActivityDate;
  String lastModifiedById;
  String lastModifiedDate;
  dynamic lastReferencedDate;
  dynamic lastViewedDate;
  double loggedHoursC;
  dynamic lpsManagerC;
  double monC;
  String name;
  String ownerId;
  String placementC;
  dynamic quantityC;
  String rateTypeC;
  dynamic recordTypeId;
  double satC;
  double serviceDeliveryManagerReportingC;
  String statusC;
  double sunC;
  String systemModstamp;
  double thuC;
  double timesheetHoursC;
  double tueC;
  dynamic unitTypeC;
  double utilisationDifferenceC;
  String utilisationC;
  double wedC;
  String weekEndDateC;
  bool x24HoursPassedC;
  List<TextEditingController> controllers = List();
  static List<String> dayTableNames = ['Sun__c','Mon__c','Tue__c','Wed__c','Thu__c','Fri__c','Sat__c'];

  TimesheetRecord({
    this.buyerGeneratedInvoiceC,
    this.candidateC,
    this.caseC,
    this.clientCodeC,
    this.commentC,
    this.createdById,
    this.createdDate,
    this.descriptionC,
    this.employeeC,
    this.expectedHoursC,
    this.friC,
    this.holidayNameC,
    this.id,
    this.invoiceStatusC,
    this.isDeleted,
    this.lastActivityDate,
    this.lastModifiedById,
    this.lastModifiedDate,
    this.lastReferencedDate,
    this.lastViewedDate,
    this.loggedHoursC,
    this.lpsManagerC,
    this.monC,
    this.name,
    this.ownerId,
    this.placementC,
    this.quantityC,
    this.rateTypeC,
    this.recordTypeId,
    this.satC,
    this.serviceDeliveryManagerReportingC,
    this.statusC,
    this.sunC,
    this.systemModstamp,
    this.thuC,
    this.timesheetHoursC,
    this.tueC,
    this.unitTypeC,
    this.utilisationDifferenceC,
    this.utilisationC,
    this.wedC,
    this.weekEndDateC,
    this.x24HoursPassedC,
    this.controllers,
  });

  List<double> hours() {
    return [sunC,monC,tueC,wedC,thuC,friC,satC];
  }

  factory TimesheetRecord.fromJson(Map<String, dynamic> json) {
    TimesheetRecord record = new TimesheetRecord(
      buyerGeneratedInvoiceC: json["Buyer_Generated_Invoice__c"],
      candidateC: json["Candidate__c"],
      caseC: json["Case__c"],
      clientCodeC: json["Client_Code__c"],
      commentC: json["Comment__c"] == null ? null : json["Comment__c"],
      createdById: json["CreatedById"],
      createdDate: json["CreatedDate"],
      descriptionC: json["Description__c"],
      employeeC: json["Employee__c"],
      expectedHoursC: json["Expected_Hours__c"],
      friC: json["Fri__c"],
      holidayNameC: json["Holiday_Name__c"],
      id: json["Id"],
      invoiceStatusC: json["InvoiceStatus__c"],
      isDeleted: json["IsDeleted"],
      lastActivityDate: json["LastActivityDate"],
      lastModifiedById: json["LastModifiedById"],
      lastModifiedDate: json["LastModifiedDate"],
      lastReferencedDate: json["LastReferencedDate"],
      lastViewedDate: json["LastViewedDate"],
      loggedHoursC: json["Logged_Hours__c"],
      lpsManagerC: json["LPS_Manager__c"],
      monC: json["Mon__c"],
      name: json["Name"],
      ownerId: json["OwnerId"],
      placementC: json["Placement__c"],
      quantityC: json["Quantity__c"],
      rateTypeC: json["Rate_Type__c"],
      recordTypeId: json["RecordTypeId"],
      satC: json["Sat__c"],
      serviceDeliveryManagerReportingC: json["Service_Delivery_Manager_Reporting__c"],
      statusC: json["Status__c"],
      sunC: json["Sun__c"],
      systemModstamp: json["SystemModstamp"],
      thuC: json["Thu__c"],
      timesheetHoursC: json["Timesheet_Hours__c"],
      tueC: json["Tue__c"],
      unitTypeC: json["Unit_Type__c"],
      utilisationDifferenceC: json["Utilisation_Difference__c"],
      utilisationC: json["Utilisation__c"],
      wedC: json["Wed__c"],
      weekEndDateC: json["Week_End_Date__c"],
      x24HoursPassedC: json["X24_Hours_Passed__c"],
      controllers: [
        TextEditingController(text: json["Sun__c"].toStringAsFixed(2)),
        TextEditingController(text: json["Mon__c"].toStringAsFixed(2)),
        TextEditingController(text: json["Tue__c"].toStringAsFixed(2)),
        TextEditingController(text: json["Wed__c"].toStringAsFixed(2)),
        TextEditingController(text: json["Thu__c"].toStringAsFixed(2)),
        TextEditingController(text: json["Fri__c"].toStringAsFixed(2)),
        TextEditingController(text: json["Sat__c"].toStringAsFixed(2)),
      ],
    );

    return record;
  }

  Map<String, dynamic> toJson() => {
    "Buyer_Generated_Invoice__c": buyerGeneratedInvoiceC,
    "Candidate__c": candidateC,
    "Case__c": caseC,
    "Client_Code__c": clientCodeC,
    "Comment__c": commentC == null ? null : commentC,
    "CreatedById": createdById,
    "CreatedDate": createdDate,
    "Description__c": descriptionC,
    "Employee__c": employeeC,
    "Expected_Hours__c": expectedHoursC,
    "Fri__c": friC,
    "Holiday_Name__c": holidayNameC,
    "Id": id,
    "InvoiceStatus__c": invoiceStatusC,
    "IsDeleted": isDeleted,
    "LastActivityDate": lastActivityDate,
    "LastModifiedById": lastModifiedById,
    "LastModifiedDate": lastModifiedDate,
    "LastReferencedDate": lastReferencedDate,
    "LastViewedDate": lastViewedDate,
    "Logged_Hours__c": loggedHoursC,
    "LPS_Manager__c": lpsManagerC,
    "Mon__c": monC,
    "Name": name,
    "OwnerId": ownerId,
    "Placement__c": placementC,
    "Quantity__c": quantityC,
    "Rate_Type__c": rateTypeC,
    "RecordTypeId": recordTypeId,
    "Sat__c": satC,
    "Service_Delivery_Manager_Reporting__c": serviceDeliveryManagerReportingC,
    "Status__c": statusC,
    "Sun__c": sunC,
    "SystemModstamp": systemModstamp,
    "Thu__c": thuC,
    "Timesheet_Hours__c": timesheetHoursC,
    "Tue__c": tueC,
    "Unit_Type__c": unitTypeC,
    "Utilisation_Difference__c": utilisationDifferenceC,
    "Utilisation__c": utilisationC,
    "Wed__c": wedC,
    "Week_End_Date__c": weekEndDateC,
    "X24_Hours_Passed__c": x24HoursPassedC,
  };
}
