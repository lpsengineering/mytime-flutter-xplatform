// To parse this JSON data, do
//
//     final placement = placementFromJson(jsonString);

import 'dart:convert';

List<Placement> placementsFromJson(String str) {
  List<Placement> placements = List();
  final jsonData = json.decode(str);
  for(Map<String, dynamic> record in jsonData["records"]) {
    placements.add(Placement.fromJson(record));
  }
  return placements.reversed.toList();
}

String placementToJson(Placement data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class Placement {
  bool activeStaffMemberC;
  String affinityEmployeeNumberC;
  dynamic clientLineManagerC;
  String contactIdC;
  dynamic contractNoticePeriodOfferC;
  String contractNoticePeriodUnitOfferC;
  String createdById;
  String createdDate;
  String endDateOfferC;
  dynamic expectedLoggedHoursC;
  String id;
  bool internalOrNonBillableJobC;
  bool isDeleted;
  dynamic lastActivityDate;
  String lastModifiedById;
  String lastModifiedDate;
  dynamic lastReferencedDate;
  dynamic lastViewedDate;
  String latestTimesheetWeekEndDateCheckC;
  String latestTimesheetWeekEndDateC;
  dynamic locationOfferC;
  dynamic lpsExecutiveOwnerC;
  String name;
  double payRateOfferC;
  String placementNameC;
  double placementTotalHoursC;
  dynamic poNumberC;
  String projectNameOfferC;
  String rateTypeOfferC;
  String rateTypeC;
  String recordTypeId;
  String relatedOfferC;
  dynamic soWContractEndDateOfferC;
  dynamic soWContractStartDateOfferC;
  String staffTypeC;
  String startDateOfferC;
  String systemModstamp;
  double targetWeeklyWorkHours2C;
  String timesheetApproverOfferC;
  dynamic timesheetApproverC;
  double totalWeeklyWorkhoursOverC;
  double totalWeeklyWorkhoursUnderC;
  String ts2AccountsPayableC;
  String ts2ClientC;
  String ts2CodeC;
  String ts2ContactEmailC;
  dynamic ts2ContactPhoneC;
  dynamic ts2ContactC;
  String ts2EmployeeC;
  dynamic ts2EndDateC;
  String ts2HiringManagerC;
  String ts2JobC;
  dynamic ts2LocationC;
  dynamic ts2PayRateC;
  dynamic ts2StartDateC;
  String ts2StatusC;
  bool ts2ThirdPartyC;
  dynamic workingDaysC;

  Placement({
    this.activeStaffMemberC,
    this.affinityEmployeeNumberC,
    this.clientLineManagerC,
    this.contactIdC,
    this.contractNoticePeriodOfferC,
    this.contractNoticePeriodUnitOfferC,
    this.createdById,
    this.createdDate,
    this.endDateOfferC,
    this.expectedLoggedHoursC,
    this.id,
    this.internalOrNonBillableJobC,
    this.isDeleted,
    this.lastActivityDate,
    this.lastModifiedById,
    this.lastModifiedDate,
    this.lastReferencedDate,
    this.lastViewedDate,
    this.latestTimesheetWeekEndDateCheckC,
    this.latestTimesheetWeekEndDateC,
    this.locationOfferC,
    this.lpsExecutiveOwnerC,
    this.name,
    this.payRateOfferC,
    this.placementNameC,
    this.placementTotalHoursC,
    this.poNumberC,
    this.projectNameOfferC,
    this.rateTypeOfferC,
    this.rateTypeC,
    this.recordTypeId,
    this.relatedOfferC,
    this.soWContractEndDateOfferC,
    this.soWContractStartDateOfferC,
    this.staffTypeC,
    this.startDateOfferC,
    this.systemModstamp,
    this.targetWeeklyWorkHours2C,
    this.timesheetApproverOfferC,
    this.timesheetApproverC,
    this.totalWeeklyWorkhoursOverC,
    this.totalWeeklyWorkhoursUnderC,
    this.ts2AccountsPayableC,
    this.ts2ClientC,
    this.ts2CodeC,
    this.ts2ContactEmailC,
    this.ts2ContactPhoneC,
    this.ts2ContactC,
    this.ts2EmployeeC,
    this.ts2EndDateC,
    this.ts2HiringManagerC,
    this.ts2JobC,
    this.ts2LocationC,
    this.ts2PayRateC,
    this.ts2StartDateC,
    this.ts2StatusC,
    this.ts2ThirdPartyC,
    this.workingDaysC,
  });

  factory Placement.fromJson(Map<String, dynamic> json) => new Placement(
    activeStaffMemberC: json["Active_Staff_Member__c"],
    affinityEmployeeNumberC: json["Affinity_Employee_Number__c"],
    clientLineManagerC: json["Client_Line_Manager__c"],
    contactIdC: json["Contact_ID__c"],
    contractNoticePeriodOfferC: json["Contract_Notice_Period_Offer__c"],
    contractNoticePeriodUnitOfferC: json["Contract_Notice_Period_Unit_Offer__c"],
    createdById: json["CreatedById"],
    createdDate: json["CreatedDate"],
    endDateOfferC: json["End_Date_Offer__c"],
    expectedLoggedHoursC: json["Expected_Logged_Hours__c"],
    id: json["Id"],
    internalOrNonBillableJobC: json["Internal_or_Non_Billable_Job__c"],
    isDeleted: json["IsDeleted"],
    lastActivityDate: json["LastActivityDate"],
    lastModifiedById: json["LastModifiedById"],
    lastModifiedDate: json["LastModifiedDate"],
    lastReferencedDate: json["LastReferencedDate"],
    lastViewedDate: json["LastViewedDate"],
    latestTimesheetWeekEndDateCheckC: json["Latest_Timesheet_Week_End_Date_Check__c"],
    latestTimesheetWeekEndDateC: json["Latest_Timesheet_Week_End_Date__c"],
    locationOfferC: json["Location_Offer__c"],
    lpsExecutiveOwnerC: json["LPS_Executive_Owner__c"],
    name: json["Name"],
    payRateOfferC: json["Pay_Rate_Offer__c"],
    placementNameC: json["Placement_Name__c"],
    placementTotalHoursC: json["Placement_Total_Hours__c"],
    poNumberC: json["PO_Number__c"],
    projectNameOfferC: json["Project_Name_Offer__c"],
    rateTypeOfferC: json["Rate_Type_Offer__c"],
    rateTypeC: json["Rate_Type__c"],
    recordTypeId: json["RecordTypeId"],
    relatedOfferC: json["Related_Offer__c"],
    soWContractEndDateOfferC: json["SoW_Contract_End_Date_Offer__c"],
    soWContractStartDateOfferC: json["SoW_Contract_Start_Date_Offer__c"],
    staffTypeC: json["Staff_Type__c"],
    startDateOfferC: json["Start_Date_Offer__c"],
    systemModstamp: json["SystemModstamp"],
    targetWeeklyWorkHours2C: json["Target_Weekly_Work_Hours2__c"],
    timesheetApproverOfferC: json["Timesheet_Approver_Offer__c"],
    timesheetApproverC: json["Timesheet_Approver__c"],
    totalWeeklyWorkhoursOverC: json["TotalWeeklyWorkhoursOver__c"],
    totalWeeklyWorkhoursUnderC: json["TotalWeeklyWorkhoursUnder__c"].toDouble(),
    ts2AccountsPayableC: json["ts2__Accounts_Payable__c"],
    ts2ClientC: json["ts2__Client__c"],
    ts2CodeC: json["ts2__Code__c"],
    ts2ContactEmailC: json["ts2__Contact_Email__c"],
    ts2ContactPhoneC: json["ts2__Contact_Phone__c"],
    ts2ContactC: json["ts2__Contact__c"],
    ts2EmployeeC: json["ts2__Employee__c"],
    ts2EndDateC: json["ts2__End_Date__c"],
    ts2HiringManagerC: json["ts2__Hiring_Manager__c"],
    ts2JobC: json["ts2__Job__c"],
    ts2LocationC: json["ts2__Location__c"],
    ts2PayRateC: json["ts2__Pay_Rate__c"],
    ts2StartDateC: json["ts2__Start_Date__c"],
    ts2StatusC: json["ts2__Status__c"],
    ts2ThirdPartyC: json["ts2__Third_Party__c"],
    workingDaysC: json["Working_Days__c"],
  );

  Map<String, dynamic> toJson() => {
    "Active_Staff_Member__c": activeStaffMemberC,
    "Affinity_Employee_Number__c": affinityEmployeeNumberC,
    "Client_Line_Manager__c": clientLineManagerC,
    "Contact_ID__c": contactIdC,
    "Contract_Notice_Period_Offer__c": contractNoticePeriodOfferC,
    "Contract_Notice_Period_Unit_Offer__c": contractNoticePeriodUnitOfferC,
    "CreatedById": createdById,
    "CreatedDate": createdDate,
    "End_Date_Offer__c": endDateOfferC,
    "Expected_Logged_Hours__c": expectedLoggedHoursC,
    "Id": id,
    "Internal_or_Non_Billable_Job__c": internalOrNonBillableJobC,
    "IsDeleted": isDeleted,
    "LastActivityDate": lastActivityDate,
    "LastModifiedById": lastModifiedById,
    "LastModifiedDate": lastModifiedDate,
    "LastReferencedDate": lastReferencedDate,
    "LastViewedDate": lastViewedDate,
    "Latest_Timesheet_Week_End_Date_Check__c": latestTimesheetWeekEndDateCheckC,
    "Latest_Timesheet_Week_End_Date__c": latestTimesheetWeekEndDateC,
    "Location_Offer__c": locationOfferC,
    "LPS_Executive_Owner__c": lpsExecutiveOwnerC,
    "Name": name,
    "Pay_Rate_Offer__c": payRateOfferC,
    "Placement_Name__c": placementNameC,
    "Placement_Total_Hours__c": placementTotalHoursC,
    "PO_Number__c": poNumberC,
    "Project_Name_Offer__c": projectNameOfferC,
    "Rate_Type_Offer__c": rateTypeOfferC,
    "Rate_Type__c": rateTypeC,
    "RecordTypeId": recordTypeId,
    "Related_Offer__c": relatedOfferC,
    "SoW_Contract_End_Date_Offer__c": soWContractEndDateOfferC,
    "SoW_Contract_Start_Date_Offer__c": soWContractStartDateOfferC,
    "Staff_Type__c": staffTypeC,
    "Start_Date_Offer__c": startDateOfferC,
    "SystemModstamp": systemModstamp,
    "Target_Weekly_Work_Hours2__c": targetWeeklyWorkHours2C,
    "Timesheet_Approver_Offer__c": timesheetApproverOfferC,
    "Timesheet_Approver__c": timesheetApproverC,
    "TotalWeeklyWorkhoursOver__c": totalWeeklyWorkhoursOverC,
    "TotalWeeklyWorkhoursUnder__c": totalWeeklyWorkhoursUnderC,
    "ts2__Accounts_Payable__c": ts2AccountsPayableC,
    "ts2__Client__c": ts2ClientC,
    "ts2__Code__c": ts2CodeC,
    "ts2__Contact_Email__c": ts2ContactEmailC,
    "ts2__Contact_Phone__c": ts2ContactPhoneC,
    "ts2__Contact__c": ts2ContactC,
    "ts2__Employee__c": ts2EmployeeC,
    "ts2__End_Date__c": ts2EndDateC,
    "ts2__Hiring_Manager__c": ts2HiringManagerC,
    "ts2__Job__c": ts2JobC,
    "ts2__Location__c": ts2LocationC,
    "ts2__Pay_Rate__c": ts2PayRateC,
    "ts2__Start_Date__c": ts2StartDateC,
    "ts2__Status__c": ts2StatusC,
    "ts2__Third_Party__c": ts2ThirdPartyC,
    "Working_Days__c": workingDaysC,
  };
}
