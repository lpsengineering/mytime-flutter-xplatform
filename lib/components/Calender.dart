import 'package:flutter/material.dart';
import 'CalenderCell.dart';

class Calender extends StatelessWidget {
  final EdgeInsets margin;
  DateTime selectedDate;
  final List<String> week = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
  final List arrayDay = [];
  Calender({this.margin, this.selectedDate});

  int totaldays(int month) {
    if (month == 2)
      return (28);
    else if (month == 4 || month == 6 || month == 9 || month == 11)
      return (30);
    else
      return (31);
  }

  @override
  Widget build(BuildContext context) {
    int element = this.selectedDate.day - this.selectedDate.weekday;
    int totalDay = totaldays(this.selectedDate.month);
    for (var i = 0; i < 7; i++) {
      if (element > totalDay) element = 1;
      arrayDay.add(element);
      element++;
    }
    var i = -1;
    return (new Container(
      margin: margin,
      alignment: Alignment.center,
      decoration: new BoxDecoration(
        color: Colors.white12,
        boxShadow: [
          BoxShadow(
            spreadRadius: 2,
            blurRadius: 4,
            color: Colors.black26,
          )
        ]
      ),
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: week.map((String week) {
          ++i;
          return new CalenderCell(
            onSelect: (() {
              selectedDate = DateTime.now();
            }),
            week: week,
            day: arrayDay[i].toString(),
            selected: arrayDay[i] == selectedDate.day);
        }).toList()),
    ));
  }


}
