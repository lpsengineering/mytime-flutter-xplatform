import 'package:flutter/material.dart';
import 'package:date_utils/date_utils.dart';

class CalendarTile extends StatelessWidget {
  final VoidCallback onDateSelected;
  final DateTime date;
  final String dayOfWeek;
  final bool isDayOfWeek;
  final bool isSelected;
  final TextStyle dayOfWeekStyles;
  final TextStyle dateStyles;
  final Widget child;

  CalendarTile({
    this.onDateSelected,
    this.date,
    this.child,
    this.dateStyles,
    this.dayOfWeek,
    this.dayOfWeekStyles,
    this.isDayOfWeek: false,
    this.isSelected: false,
  });

  Widget renderDateOrDayOfWeek(BuildContext context) {
    if (isDayOfWeek) {
      return new InkWell(
        child: new Container(
          alignment: Alignment.center,
          child: new Text(
            dayOfWeek.toUpperCase(),
//            style: dayOfWeekStyles,
            style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
                color: isSelected ? Colors.black87 : Colors.black26
            )
          ),
        ),
      );
    } else {
      return new InkWell(
        onTap: onDateSelected,
        child: new Container(
          alignment: Alignment.center,
          child: new Text(
            Utils.formatDay(date).toString(),
//            style: isSelected ? Theme.of(context).primaryTextTheme.body1 : dateStyles,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.w500,
              color: isSelected ? Colors.black87 : Colors.black26
            )
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    if (child != null) {
      return Container(
        child:  InkWell(
          child: child,
          onTap: onDateSelected,
        ),
      );
    }
    return new Container(

      decoration: BoxDecoration(
        color: isSelected ? Colors.white : Colors.transparent,
        boxShadow: isSelected ? [
          BoxShadow(
            color: Colors.black12,
            spreadRadius: 3,
            blurRadius: 1,
            offset: Offset(0, 4)
          )
        ] : [],
      ),
      child: renderDateOrDayOfWeek(context),
    );
  }
}