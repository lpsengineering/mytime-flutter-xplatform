import 'package:flutter/material.dart';
import 'package:tuple/tuple.dart';
import 'package:mytime_mobile/components/calendar/calendar.dart';

class ListViewContent extends StatelessWidget {
  final ValueChanged<DateTime> onDateSelected;
  final ValueChanged<Tuple2<DateTime, DateTime>> onSelectedRangeChanged;
  final List<Widget> listContents;
  ListViewContent({
    this.onDateSelected,
    this.onSelectedRangeChanged,
    this.listContents,
  });
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Calendar(
          isExpandable: false,
          showTodayAction: false,
          showChevronsToChangeRange: true,
          showCalendarPickerIcon: false,
          onDateSelected: onDateSelected,
          onSelectedRangeChange: onSelectedRangeChanged,
        ),
        Padding(
          padding: EdgeInsets.only(top: 8),
        )
      ]..addAll(listContents)
    );
  }
}