import 'package:flutter/material.dart';
import 'MonthView.dart';
import 'Profile_Notification.dart';
import 'package:mytime_mobile/utils/colors.dart';
import 'package:mytime_mobile/services/globals.dart' as globals;

class ImageBackground extends StatelessWidget {
  final DecorationImage backgroundImage;
  final DecorationImage profileImage;
  final VoidCallback selectbackward;
  final VoidCallback selectforward;
  final String month;
  final String name;


  ImageBackground(
      {this.backgroundImage,
      this.profileImage,
      this.month,
      this.selectbackward,
      this.selectforward,
      this.name,
      });

  Widget greeterText() {
    return Text('myTime',
      style: new TextStyle(
          fontSize: 14.0,
          letterSpacing: 1.2,
          fontWeight: FontWeight.w300,
          color: Colors.white54),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return (new Container(
        width: screenSize.width,
        height: 300,
        decoration: new BoxDecoration(
          image: backgroundImage,
          color: HexColor('#01579B')
        ),
        child: new Container(
          padding: EdgeInsets.only(top:20),
          child: new Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(20, 30, 0, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              greeterText(),
                              Text(name,
                                style: new TextStyle(
                                    fontSize: 25.0,
                                    letterSpacing: 1.2,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.white),
                              ),
                            ],
                          ),
                        ],
                      )
                    ),
                    Padding(padding: EdgeInsets.only(top:10)),
                    ProfileNotification(
                      profileImage: profileImage,
                    ),
                  ],
                ),
        )));
  }
}
