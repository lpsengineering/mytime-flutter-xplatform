import 'package:flutter/material.dart';

class ProfileNotification extends StatelessWidget {
  final DecorationImage profileImage;
  ProfileNotification({this.profileImage});
  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 8,
      shape: CircleBorder(),
      child: Container(
          height: 120,
          width: 120,
          child: new Column(
            children: [
//              new Container(
//                  margin: new EdgeInsets.only(left: 80.0),
//                  width: 30,
//                  height: 30,
//                  child: new Center(
//                    child: new Text("8",
//                        style: new TextStyle(
//                            fontWeight: FontWeight.w400,
//                            color: Colors.white)),
//                  ),
//                  decoration: new BoxDecoration(
//                    shape: BoxShape.circle,
//                    color: Colors.lightGreen,
//                  )),
            ],
          ),
          decoration: new BoxDecoration(
            shape: BoxShape.circle,
            image: profileImage,
          )
      )
    );
  }
}
