import 'package:flutter/material.dart';

class CalenderCell extends StatelessWidget {
  final String week;
  final String day;
  final bool selected;
  final VoidCallback onSelect;
  CalenderCell({this.week, this.day, this.selected, this.onSelect});
  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
        child: Container(
      color: selected ? Colors.white : Colors.white30,
      child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(padding: EdgeInsets.only(top: 10)),
        new Text(
          week,
          style: new TextStyle(
              color: selected ? Colors.black87 : Colors.black26,
              fontSize: 14.0,
              fontWeight: FontWeight.w400),
        ),
        new Padding(
          padding: new EdgeInsets.only(bottom: 4.0),
          child: new Container(
              width: 35.0,
              height: 35.0,
              alignment: Alignment.center,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(
                    day,
                    style: new TextStyle(
                      fontSize: 24.0,
                      fontWeight: FontWeight.w500,
                      color: selected ? Colors.black87 : Colors.black26
                    ),
                  ),
                ],
              )),
        )
      ],
      )
      )
    );
  }
}
