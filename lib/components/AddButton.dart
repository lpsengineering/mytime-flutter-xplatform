import 'package:flutter/material.dart';

class AddButton extends StatelessWidget {
  AddButton();
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 60,
      height: 60,
      alignment: FractionalOffset.bottomRight,
      decoration: BoxDecoration(
          color: Colors.lightBlue,
          shape: BoxShape.circle),
      child: Center(
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
    );
  }
}
