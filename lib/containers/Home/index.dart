import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/animation.dart';
import '../../components/ListViewContainer.dart';
import '../../components/HomeTopView.dart';
import 'package:intl/intl.dart';
import 'package:flutter/scheduler.dart' show timeDilation;
import 'package:mytime_mobile/utils/colors.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:mytime_mobile/services/prefs.dart' as prefs;
import 'package:mytime_mobile/models/auth.dart';
import 'package:mytime_mobile/models/timesheet.dart';
import 'package:mytime_mobile/models/placement_code.dart';
import 'package:mytime_mobile/models/placement.dart';
import 'package:mytime_mobile/models/user.dart';
import 'package:mytime_mobile/services/api.dart';
import 'package:mytime_mobile/containers/ReviewTimesheet/index.dart';
import 'package:date_utils/date_utils.dart';
import 'package:mytime_mobile/utils/date.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:mytime_mobile/utils/transparent_route.dart';
import 'package:lottie_flutter/lottie_flutter.dart';
import 'package:flutter/services.dart' show rootBundle;

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  HomePageState createState() => new HomePageState();
}

class HomePageState extends State<HomePage> with TickerProviderStateMixin {
  AnimationController _screenController;
  AnimationController _buttonController;
  AnimationController _loaderController;
  LottieComposition _composition;
  Animation<double> listTileWidth;

  ScrollController _scrollController;
  double _mainOpacity = 1;

  UserProfile _userProfile;
  SfAuthResponse _auth;

  DateTime _currentDate = DateTime.now();
  List<Placement> _placements = List();
  List<PlacementCode> _placementCodes = List();
  Timesheet _timesheet;

  TextEditingController _descriptionController;
  TextEditingController _commentController;

  TimesheetRecord _selectedRecord;

  var animateStatus = 0;
  List<String> months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  String month = new DateFormat.MMMM().format(
    new DateTime.now(),
  );
  int index = new DateTime.now().month;
  void _selectforward() {
    if (index < 12)
      setState(() {
        ++index;
        month = months[index - 1];
      });
  }

  void _selectbackward() {
    if (index > 1)
      setState(() {
        --index;
        month = months[index - 1];
      });
  }

  @override
  void initState() {
    super.initState();
    _loaderController = new AnimationController(
      duration: const Duration(seconds: 4),
      vsync: this,
    );
    _loaderController.addListener(() => setState(() {}));
//    asyncLoadAssets();
    asyncInit();
    _scrollController = ScrollController();
    _scrollController.addListener(scrollListener);
  }

  Future<void> asyncLoadAssets() async {
    await loadAsset('assets/lottie/loading.json').then((LottieComposition composition) {
      setState(() {
        _composition = composition;
        _loaderController.reset();
        _loaderController.repeat();
      });
    });
    showDialog(
        context: context,
        builder: (context) {
          return Center(
            child: Container(
              child: Lottie(
                composition: _composition,
                size: const Size(300.0, 300.0),
                controller: _loaderController,
              ),
            ),
          );
        }
    );
    await asyncInit();
  }

  Future<void> asyncInit() async {
    _currentDate = DateTime.now();
    await loadUserProfile();
    setState(() {
      prefs.getAuthFromPrefs().then((value) {
        _auth = value;
      });
    });
    this._placements = await getPlacements(contactId: _userProfile.contactIdC);
    this._placementCodes = await getPlacementCodes();
    var lastDayOfCurrentWeek = Utils.lastDayOfWeek(_currentDate);
    await getTimesheet(date: lastDayOfCurrentWeek, userId: _userProfile.id);
    startAnimations();
    return;
  }

  Future<LottieComposition> loadAsset(String assetName) async {
    return await rootBundle
        .loadString(assetName)
        .then<Map<String, dynamic>>((String data) => json.decode(data))
        .then((Map<String, dynamic> map) => new LottieComposition.fromMap(map));
  }

  updateHourForDay({@required TimesheetRecord record, @required int dayIndex,
      @required String value}) async {
    await setHourForDay(
        timesheetId: record.id,
        day: TimesheetRecord.dayTableNames[dayIndex],
        hours: double.parse(value),
    );
  }

  scrollListener() {
    if (_scrollController.offset >= _scrollController.position.maxScrollExtent) {
      setState(() {
//        _mainOpacity = 0;
      });
    }
  }

  loadUserProfile() async {
    UserProfile profile = await getUserDetails();
    setState(() {
      _userProfile = profile;
    });
  }

  Future<void> getTimesheet({ @required DateTime date,
      @required String userId }) async {
    DateFormat format = DateFormat('yyyy-MM-dd');
    var timesheet = await getTimesheetForWeekEnd(format.format(date),
        userId: userId);
    setState(() {
      _timesheet = timesheet;
    });
    return;
  }

  List<Widget> listContents(int day) {
    List<Widget> listContents = List();
    if (_timesheet == null) {
      return listContents;
    }
    this._timesheet.records.forEach((timesheet) {
      listContents.insert(0, timesheetItem(timesheet, day));
    });
    return listContents;
  }

  Widget timesheetItem(TimesheetRecord timesheet, int day) {
    Placement placement = _placements.firstWhere( (query) {
      return query.id == timesheet.placementC;
    });
    return _selectedRecord == timesheet ?
    Padding(
      padding: EdgeInsets.only(bottom: 40, left: 8, right: 8),
      child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: Colors.white
          ),
          height: 300,
          child: Stack(children: [
            Row(
            children: <Widget>[
              Padding(padding: EdgeInsets.only(left: 8)),
              Expanded(
                flex: 1,
                child: Padding(
                  padding: EdgeInsets.all(8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(padding: EdgeInsets.only(top: 8)),
                      Text("PROJECT", style: TextStyle(
                        color: Colors.black26,
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                      )),
                      DropdownButton(
                        value: placement,
                        isExpanded: true,
                        items: _placements.map((plc) {
                          return DropdownMenuItem(
                            value: plc,
                            child: Text(plc.placementNameC),
                          );
                        }).toList(),
                        onChanged: (Placement item){
                          setState((){
                            timesheet.placementC = item.id;
                          });
                          updateTimesheet(timesheet: timesheet);
                        },
                      ),
                      Text("PLACEMENT CODE", style: TextStyle(
                        color: Colors.black26,
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                      )),
                      DropdownButton(
                        value: _placementCodes.firstWhere((code) =>
                          code.name == timesheet.clientCodeC
                        ),
                        isExpanded: true,
                        items: _placementCodes.map((plc) {
                          return DropdownMenuItem(
                            value: plc,
                            child: Text(plc.name),
                          );
                        }).toList(),
                        onChanged: (PlacementCode item) {
                          setState(() {
                            timesheet.clientCodeC = item.name;
                          });
                          updateTimesheet(timesheet: timesheet);
                        },
                      ),
                      Text("DESCRIPTION", style: TextStyle(
                        color: Colors.black26,
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                      )),
                      TextField(
                        key: Key('tf_Desc_Key'),
                        onSubmitted: (text){
                          setState(() {
                            timesheet.descriptionC = text;
                          });
                          updateTimesheet(timesheet: timesheet);
                        },
                        textInputAction: TextInputAction.done,
                        textCapitalization: TextCapitalization.sentences,
                        maxLines: 1,
                        controller: _descriptionController =
                          TextEditingController(text:
                            timesheet.descriptionC == null ? '' :
                            timesheet.descriptionC),
                      ),
                      Padding(padding: EdgeInsets.only(top: 6)),
                      Text("COMMENT", style: TextStyle(
                        color: Colors.black26,
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                      )),
                      TextField(
                        onSubmitted: (text) {
                          setState(() {
                            timesheet.commentC = text;
                          });
                          updateTimesheet(timesheet: timesheet);
                        },
                        textInputAction: TextInputAction.done,
                        textCapitalization: TextCapitalization.sentences,
                        maxLines: 1,
                        controller: _commentController =
                            TextEditingController(text:
                            timesheet.commentC == null ? '' :
                            timesheet.commentC),
                      ),

                    ]
                  )
                )
              )
            ],
          ),
          Container(
            alignment: Alignment.bottomRight,
            transform: Matrix4.translationValues(-8, 25, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                    width: 45,
                    height: 45,
                    decoration: BoxDecoration(
                        color: Colors.red,
                        shape: BoxShape.circle
                    ),
                    child: FloatingActionButton(
                      backgroundColor: Colors.green,
                      elevation: 6,
                      child: Icon(Icons.arrow_drop_up,
                          color: Colors.white),
                      onPressed: (){
                        setState(() {
                          _selectedRecord = null;
                        });
                      },
                    )
                ),
              ],
            )
          )
        ]
        )
      ),
    )
    :
    Padding(
      padding: EdgeInsets.only(bottom: 8, left: 8, right: 8),
      child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4),
              color: Colors.white
          ),
          height: 80,
          child: Row(
            children: <Widget>[
              Container(
                width: 80,
                alignment: Alignment.center,
                color: Colors.blue,
                child: TextField(
                    controller: timesheet.controllers[day],
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                            borderSide: new BorderSide(
                                color: Colors.transparent
                            )
                        )
                    ),
                    keyboardType: TextInputType.number,
                    onTap: () {
                      timesheet.controllers[day].selection =
                          TextSelection(
                              baseOffset: 0,
                              extentOffset:
                              timesheet.controllers[day].text.length
                          );
                    },
                    textInputAction: TextInputAction.done,
                    enableInteractiveSelection: true,
                    onChanged: (value){
                      if (value.length > 0) {
                        FocusScope.of(context).requestFocus(new FocusNode());
                        this.updateHourForDay(record: timesheet,
                            dayIndex: day,
                            value: value
                        );
                      }
                    },
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    )
                ),
              ),
              Padding(padding: EdgeInsets.only(left: 8)),
              Expanded(
                flex: 1,
                child: InkWell(
                  splashColor: Colors.blue,
                  onTap: (){
                    setState(() {
                      _selectedRecord = timesheet;
                    });
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(placement != null ? placement.placementNameC : '',
                          textAlign: TextAlign.left,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.black87,
                              fontSize: 18,
                              fontWeight: FontWeight.w500
                          )
                      ),
                      Text(timesheet.clientCodeC == null ? '-- None --'
                          : timesheet.clientCodeC,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Colors.black54,
                            fontSize: 14,
                          )
                      ),
                      Text(timesheet.descriptionC == null ? '' :
                      timesheet.descriptionC,
                          key: Key('txt_Desc_Key'),
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Colors.black54,
                            fontSize: 14,
                          )
                      ),
                    ]
                  )
                )
              )
            ],
          )
      ),
    );
  }

  hideMainAndShowReview() async {
    setState((){
      _mainOpacity = 0;
    });
    await Navigator.of(context).push(
        TransparentRoute(
          builder: (context) =>
            ReviewTimesheetPage(timesheet: _timesheet,)
        )
    );
    setState((){
      _mainOpacity = 1;
    });
  }

  startAnimations() {
    _screenController = new AnimationController(
        duration: new Duration(milliseconds: 800), vsync: this);
    _buttonController = new AnimationController(
        duration: new Duration(milliseconds: 500), vsync: this);

    listTileWidth = new Tween<double>(
      begin: 1000.0,
      end: 600.0,
    )
        .animate(
      new CurvedAnimation(
        parent: _screenController,
        curve: new Interval(
          0.225,
          0.600,
          curve: Curves.bounceIn,
        ),
      ),
    );

    _screenController.forward();
  }

  @override
  void dispose() {
    _screenController.dispose();
    _buttonController.dispose();
    super.dispose();
  }

  checkLocationPermission() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.location);
    if (permission != PermissionStatus.granted) {
      await PermissionHandler().requestPermissions([
        PermissionGroup.location,
        PermissionGroup.locationAlways,
        PermissionGroup.locationWhenInUse
      ]);
    }
  }
  Timer _debounce;

  @override
  Widget build(BuildContext context) {
    timeDilation = 0.3;
    return WillPopScope(
      onWillPop: () async {
        return true;
      },
      child: new Scaffold(
        body: new Container(
          decoration: BoxDecoration(
            color: HexColor('#01579B'),
          ),
          child:
          AnimatedOpacity(
            duration: Duration(milliseconds: 300),
            opacity: _mainOpacity,
            curve: Curves.easeOutQuad,
            child: LiquidPullToRefresh(
            notificationPredicate: (ScrollNotification notification) {
              if (notification is ScrollUpdateNotification) {
                if(notification.metrics.outOfRange &&
                    notification.metrics.pixels -
                    notification.metrics.maxScrollExtent > 80) {

                  if (_debounce?.isActive ?? false) _debounce.cancel();
                  _debounce = Timer(const Duration(milliseconds: 25), () {
                    hideMainAndShowReview();
                  });
                }
              } else if (notification is OverscrollNotification) {
                OverscrollNotification overscroll = notification;
                if (overscroll.overscroll > 50) {
                  if (_debounce?.isActive ?? false) _debounce.cancel();
                  _debounce = Timer(const Duration(milliseconds: 25), () {
                    hideMainAndShowReview();
                  });
                }
              }
              return true;
            },
            onRefresh: asyncInit,
            backgroundColor: HexColor('#01579B'),
            color: Colors.white,
            child:
          CustomScrollView(
            physics: BouncingScrollPhysics(),
            controller: _scrollController,
            slivers: <Widget>[
              SliverList(
                delegate: SliverChildListDelegate(
                  [
                    (_auth != null) ?
                    new ImageBackground(
                      backgroundImage: DecorationImage(
                        colorFilter: new ColorFilter.mode(
                            Colors.black.withOpacity(0.2),
                            BlendMode.dstATop
                        ),
                        image: CachedNetworkImageProvider(
                            _userProfile.bannerPhotoUrl != null ?
                            '${_auth.instanceUrl}${_userProfile.bannerPhotoUrl}' :
                            "",
                            headers: { 'Authorization': 'Bearer ${_auth.accessToken}' }
                        ),
                        fit: BoxFit.fitHeight,
                      ),
                      profileImage: DecorationImage(
                        image: CachedNetworkImageProvider(
                            _userProfile.mediumPhotoUrl != null ?
                            _userProfile.mediumPhotoUrl :
                            ""
                            ,
                            headers: { 'Authorization': 'Bearer ${_auth.accessToken}' }
                        ),
                        fit: BoxFit.fitHeight,
                      ),
                      month: month,
                      name: '${_userProfile.firstName} ${_userProfile.lastName}',
                      selectbackward: _selectbackward,
                      selectforward: _selectforward,
                    ) : Container(),
                    Container(
                      transform: Matrix4.translationValues(0.0, -50.0, 0.0),
                      child: ListViewContent(
                        onDateSelected: (date) {
                          setState((){
                            _currentDate = date;
                          });
                        },
                        onSelectedRangeChanged: (dates) {
                          var lastDayOfCurrentWeek = Utils.lastDayOfWeek(_currentDate);
                          getTimesheet(date: lastDayOfCurrentWeek, userId: _userProfile.id);
                        },
                        listContents: listContents(
                            getRealWeekendIndex(_currentDate)),
                      )
                    )
                  ],
                ),
    //              animateStatus == 0
    //                  ? new Padding(
    //                      padding: new EdgeInsets.all(20.0),
    //                      child: new InkWell(
    //                          splashColor: Colors.white,
    //                          highlightColor: Colors.white,
    //                          onTap: () {
    ////                            setState(() {
    ////                              animateStatus = 1;
    ////                            });
    ////                            _playAnimation();
    //                            Navigator.of(context).pushNamed('/editTimeLog');
    ////                            showModalBottomSheet(
    ////                            context: context,
    ////                            builder: (ctx) {
    ////                              return Container(
    ////                                child: EditTimeLogPage()
    ////                              );
    ////                            });
    //                          },
    //                          child: new AddButton(
    //                          )))
    //                  : new StaggerAnimation(
    //                      buttonController: _buttonController.view),
              ),
        ]),
          ),
      )
    )
    )
    );
  }
}
