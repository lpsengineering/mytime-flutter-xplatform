import 'package:flutter/material.dart';
import 'package:animated_background/animated_background.dart';
import 'package:mytime_mobile/models/timesheet.dart';
import 'package:mytime_mobile/utils/colors.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:charts_common/common.dart' as common;
import 'package:mytime_mobile/utils/mood_meter/main.dart';

class ReviewTimesheetPage extends StatefulWidget {
  final Timesheet timesheet;
  const ReviewTimesheetPage({ Key key, this.timesheet }) : super(key: key);
  @override
  ReviewTimesheetPageState createState() => new ReviewTimesheetPageState();
}

class ReviewTimesheetPageState extends State<ReviewTimesheetPage> with TickerProviderStateMixin {

  List<GraphData> _graphData = List();

  @override
  void initState() {
    setState(() {
      _graphData.clear();
      widget.timesheet.records.forEach( (record) {
        _graphData.add(GraphData(day: 'SUN', hours: record.sunC, totalPerDay: 8));
        _graphData.add(GraphData(day: 'MON', hours: record.monC, totalPerDay: 8));
        _graphData.add(GraphData(day: 'TUE', hours: record.tueC, totalPerDay: 8));
        _graphData.add(GraphData(day: 'WED', hours: record.wedC, totalPerDay: 8));
        _graphData.add(GraphData(day: 'THU', hours: record.thuC, totalPerDay: 8));
        _graphData.add(GraphData(day: 'FRI', hours: record.friC, totalPerDay: 8));
        _graphData.add(GraphData(day: 'SAT', hours: record.satC, totalPerDay: 8));
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        child: Stack(
          children:[
            AnimatedBackground(
              behaviour: RandomParticleBehaviour(
                options: ParticleOptions(
                  maxOpacity: 0.1,
                  minOpacity: 0.01,
                  baseColor: Colors.lightBlueAccent,
                  particleCount: 20,
                  spawnMaxSpeed: 5,
                  spawnMaxRadius: 70,
                  spawnMinRadius: 10,
                  opacityChangeRate: 0.75,
                  spawnMinSpeed: 1,
                  spawnOpacity: 0.05
                )
              ),
              vsync: this,
              child: Container(),
            ),
            Center(
              child: SizedBox.expand(
                child: Container(
                  alignment: Alignment.topCenter,
                  color: Colors.black26,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      InkWell(
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          width: double.infinity,
                          padding: EdgeInsets.all(20),
                          child: Column(
                            children: <Widget>[
                              Icon(Icons.keyboard_arrow_up, size: 30, color: Colors.white24),
                              Text('Review Timesheet',
                                  style: TextStyle(
                                      fontSize: 24,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold
                                  )
                              ),
                              Text('April 7 - April 13',
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.white54,
                                      fontWeight: FontWeight.w500
                                  )
                              )
                            ],
                          ),
                        )
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 10.0, bottom: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Text('${widget.timesheet.getTotalHours().toStringAsFixed(0)}',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 100,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'roboto'
                                      ),
                                    ),
                                    Text('/40',
                                      style: TextStyle(
                                          color: Colors.white54,
                                          fontSize: 65,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'roboto'
                                      ),
                                    ),
                                  ],
                                ),
                                Text('HOURS THIS WEEK',
                                  style: TextStyle(
                                    color: Colors.white54,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 16
                                  )
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height:260,
                            width: 260,
                            child: charts.BarChart([
                              charts.Series(
                                  id: 'Hours',
                                  domainFn: (data, _) => data.day,
                                  measureFn: (data, _) => data.hours,
                                  colorFn: (data, _) => data.color,
                                  data: _graphData
                              )
                            ],
                              animate: true,
                              animationDuration: Duration(seconds: 3),
                              domainAxis: new charts.OrdinalAxisSpec(
                                renderSpec: new charts.SmallTickRendererSpec(

                                // Tick and Label styling here.
                                labelStyle: new charts.TextStyleSpec(
                                fontSize: 10, // size in Pts.
                                color: charts.ColorUtil.fromDartColor(Colors.white54)),

                                // Change the line colors to match text color.
                                lineStyle: new charts.LineStyleSpec(
                                color: charts.ColorUtil.fromDartColor(Colors.white30)))),

                                /// Assign a custom style for the measure axis.
                                primaryMeasureAxis: new charts.NumericAxisSpec(
                                renderSpec: new charts.GridlineRendererSpec(

                                // Tick and Label styling here.
                                labelStyle: new charts.TextStyleSpec(
                                fontSize: 12, // size in Pts.
                                color: charts.ColorUtil.fromDartColor(Colors.white30)),

                                // Change the line colors to match text color.
                                lineStyle: new charts.LineStyleSpec(
                                color: charts.ColorUtil.fromDartColor(Colors.white30)))),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(8),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(12)
                            ),
                            child: Row(
                              children: <Widget>[
//                                Icon(Icons.check_circle)
                                Text('Perfect!! Your total weekly hours meets the Target hours',
                                    style: TextStyle(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 24
                                    )
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      Material(
                        color: Colors.transparent,
                        child: Container(
                            height: 90.0,
                            alignment: Alignment.center,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 10.0),
                              child: InkWell(
                                onTap: () async {
                                  var result = await Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                                    return MyReviewPage(timesheet: widget.timesheet,);
                                  }));
                                  showDialog(context: context,
                                      barrierDismissible: false,
                                      builder: (context) {
                                        return Container(
                                          child: Center(
                                            child: Container(
                                                height: 200,
                                                width: 200,
                                                child: CircularProgressIndicator(),
                                            ),
                                          ),
                                        );
                                      }
                                  );
                                  Future.delayed(Duration(seconds: 3), () {
                                    Navigator.of(context)
                                        .popUntil(
                                        ModalRoute
                                            .withName('/home'));
//                                    showDialog(
//                                      context: context,
//                                      barrierDismissible: false,
//                                      builder: (context) {
//                                        return Center(
//                                          child: Container(
//                                            child: Column(
//                                              children: <Widget>[
//                                                Text('Timesheet submitted successfully'),
//                                                Container(
//                                                  alignment: Alignment.bottomRight,
//                                                  child: FlatButton(
//                                                    onPressed: () {
//                                                      Navigator.of(context)
//                                                          .popUntil(
//                                                          ModalRoute
//                                                            .withName('/home')
//                                                      );
//                                                    },
//                                                    child: Text('Close')),
//                                                )
//                                              ],
//                                            ),
//                                          ),
//                                        );
//                                      }
//                                    );
                                  });
                                },
                                child: Material(
                                  elevation: 4,
                                  borderRadius: BorderRadius.circular(40),
                                  color: Colors.transparent,
                                  child: Container(
                                    height: 57,
                                    width: double.infinity,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(40),
                                        gradient: LinearGradient(
                                            colors: [HexColor('#7CB342'), HexColor('#6CA234')],
                                            begin: AlignmentDirectional.topCenter,
                                            end: AlignmentDirectional.bottomCenter
                                        )
                                    ),
                                    child: Text(
                                      'Continue',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ]
        ),
      )
    );
  }
}

class GraphData {
  String day;
  double hours;
  double totalPerDay;
  static Color _color = Colors.white;
  charts.Color color = charts.Color(r: _color.red, g: _color.green, b: _color.blue, a: _color.alpha);

  GraphData({this.day, this.hours, this.totalPerDay});


}